//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FlashMessanger
{
    using System;
    using System.Collections.Generic;
    
    public partial class Friends
    {
        public int UserId { get; set; }
        public int UserFriend { get; set; }
        public int Confirmed { get; set; }
        public Nullable<System.DateTime> DateOfConfirmed { get; set; }
    
        public virtual Users Users { get; set; }
        public virtual Users Users1 { get; set; }
    }
}
