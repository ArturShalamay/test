﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FlashMessanger.Controllers
{
    public class MessageController : ApiController
    {
        public IEnumerable<Users> Get()
        {

            using (dbFlashEntities entetie = new dbFlashEntities())
            {
                entetie.Configuration.ProxyCreationEnabled = false;
                //return entetie.User.ToList();
            }
        }

        public HttpResponseMessage Get(int id)
        {
            using (dbFlashEntities entetie = new dbFlashEntities())
            {
                var entity = entetie.UserPosts.All(m => m.UserId == id);
                if (entity == null)
                    return Request.CreateErrorResponse(HttpStatusCode.NotFound, $"User with id {id} not found");
                else
                    return Request.CreateResponse(HttpStatusCode.OK, entity);
            }
        }

        public HttpResponseMessage Post([FromBody]Users newUser)
        {
            try
            {
                using (dbFlashEntities entetie = new dbFlashEntities())
                {
                    entetie.Users.Add(newUser);
                    entetie.SaveChanges();

                    var message = Request.CreateResponse(HttpStatusCode.Created, newUser);
                    message.Headers.Location = new Uri(Request.RequestUri + newUser.UserId.ToString());

                    return message;
                }
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        public HttpResponseMessage Put(int id, [FromBody]Users newUser)
        {
            try
            {
                using (dbFlashEntities entetie = new dbFlashEntities())
                {
                    var entity = entetie.Users.FirstOrDefault(m => m.UserId == id);
                    if (entity == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound, $"User with id {id} not found");
                    }
                    else
                    {
                        entity.UserId = newUser.UserId;
                        entity.DateOfReg = newUser.DateOfReg;
                        entetie.SaveChanges();

                        return Request.CreateResponse(HttpStatusCode.OK, entity);
                    }

                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex);
            }
        }

        public HttpResponseMessage Delete(int id)
        {
            using (dbFlashEntities entetie = new dbFlashEntities())
            {
                try
                {
                    var enteties = entetie.Users.FirstOrDefault(m => m.UserId == id);
                    if (entetie == null)
                    {
                        return Request.CreateErrorResponse(HttpStatusCode.NotFound, $"There no such user with id {id}");
                    }
                    else
                    {
                        entetie.Users.Remove(enteties);
                        entetie.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK);
                    }
                }
                catch (Exception ex)
                {
                    return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ex);
                }
            }
        }
    }
}
